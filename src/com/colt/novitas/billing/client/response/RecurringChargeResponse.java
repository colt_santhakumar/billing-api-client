package com.colt.novitas.billing.client.response;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class RecurringChargeResponse implements Serializable {

	private static final long serialVersionUID = 4807575172807303293L;

	@SerializedName("id")
	private Integer id;
	@SerializedName("request_id")
	private Integer requestId;
	@SerializedName("service_id")
	private String serviceId;
	@SerializedName("service_instance_id")
	private String serviceInstanceId;
	@SerializedName("service_instance_type")
	private String serviceInstanceType;
	@SerializedName("bcn")
	private String bcn;
	@SerializedName("charge_type")
	private String chargeType;
	@SerializedName("description")
	private String description;
	@SerializedName("currency")
	private String currency;
	@SerializedName("frequency")
	private String frequency;
	@SerializedName("amount")
	private Float amount;
	@SerializedName("started_at")
	private String startedAt;
	@SerializedName("ended_at")
	private String endedAt;
	@SerializedName("created_at")
	private String createdAt;
	@SerializedName("status")
	private String status;
	@SerializedName("batch_id")
	private String batchId;
	@SerializedName("updated_at")
	private String updatedAt;
	@SerializedName("commitment_type")
	private String commitmentType;
	@SerializedName("commitment_expiry_date")
	private String commitmentExpiryDate;

	public RecurringChargeResponse() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceInstanceId() {
		return serviceInstanceId;
	}

	public void setServiceInstanceId(String serviceInstanceId) {
		this.serviceInstanceId = serviceInstanceId;
	}

	public String getServiceInstanceType() {
		return serviceInstanceType;
	}

	public void setServiceInstanceType(String serviceInstanceType) {
		this.serviceInstanceType = serviceInstanceType;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getChargeType() {
		return chargeType;
	}


	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public String getStartedAt() {
		return startedAt;
	}

	public void setStartedAt(String startedAt) {
		this.startedAt = startedAt;
	}

	public String getEndedAt() {
		return endedAt;
	}

	public void setEndedAt(String endedAt) {
		this.endedAt = endedAt;
	}


	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}


	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCommitmentType() {
		return commitmentType;
	}

	public void setCommitmentType(String commitmentType) {
		this.commitmentType = commitmentType;
	}

	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}

	public RecurringChargeResponse(Integer id, Integer requestId, String serviceId, String serviceInstanceId,
			String serviceInstanceType, String bcn, String chargeType, String description, String currency,
			String frequency, Float amount, String startedAt, String endedAt, String createdAt, String status,
			String batchId, String updatedAt, String commitmentType, String commitmentExpiryDate) {
		super();
		this.id = id;
		this.requestId = requestId;
		this.serviceId = serviceId;
		this.serviceInstanceId = serviceInstanceId;
		this.serviceInstanceType = serviceInstanceType;
		this.bcn = bcn;
		this.chargeType = chargeType;
		this.description = description;
		this.currency = currency;
		this.frequency = frequency;
		this.amount = amount;
		this.startedAt = startedAt;
		this.endedAt = endedAt;
		this.createdAt = createdAt;
		this.status = status;
		this.batchId = batchId;
		this.updatedAt = updatedAt;
		this.commitmentType = commitmentType;
		this.commitmentExpiryDate = commitmentExpiryDate;
	}

	@Override
	public String toString() {
		return "RecurringChargeResponse [id=" + id + ", requestId=" + requestId + ", serviceId=" + serviceId
				+ ", serviceInstanceId=" + serviceInstanceId + ", serviceInstanceType=" + serviceInstanceType + ", bcn="
				+ bcn + ", chargeType=" + chargeType + ", description=" + description + ", currency=" + currency
				+ ", frequency=" + frequency + ", amount=" + amount + ", startedAt=" + startedAt + ", endedAt="
				+ endedAt + ", createdAt=" + createdAt + ", status=" + status + ", batchId=" + batchId + ", updatedAt="
				+ updatedAt + ", commitmentType=" + commitmentType + ", commitmentExpiryDate=" + commitmentExpiryDate
				+ "]";
	}

}
