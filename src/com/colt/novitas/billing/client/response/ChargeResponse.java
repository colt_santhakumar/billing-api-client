package com.colt.novitas.billing.client.response;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class ChargeResponse implements Serializable {

	private static final long serialVersionUID = -5730837184983475917L;
	
	@SerializedName("status_id")
	private Integer statusId;
	@SerializedName("description")
	private String Description;

	public ChargeResponse() {
		super();
	}

	public ChargeResponse(Integer statusId, String description) {
		super();
		this.statusId = statusId;
		Description = description;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}
	
	@Override
	public String toString() {
		return "ChargeResponse [statusId=" + statusId + ", Description=" + Description + "]";
	}
}
