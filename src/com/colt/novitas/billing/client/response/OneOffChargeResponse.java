package com.colt.novitas.billing.client.response;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class OneOffChargeResponse implements Serializable {

	private static final long serialVersionUID = 8607523472526356481L;
	
	@SerializedName("id")
	private Integer id;
	@SerializedName("request_id")
	private Integer requestId;
	@SerializedName("service_id")
	private String serviceId;
	@SerializedName("service_instance_id")
	private String serviceInstanceId;
	@SerializedName("service_instance_type")
	private String serviceInstanceType;
	@SerializedName("bcn")
	private String bcn;
	@SerializedName("charge_type")
	private String chargeType;
	@SerializedName("description")
	private String description;
	@SerializedName("currency")
	private String currency;
	@SerializedName("amount")
	private Float amount;
	@SerializedName("created_at")
	private String createdAt;
	@SerializedName("status")
	private String status;
	@SerializedName("batch_id")
	private String batchId;
	@SerializedName("updated_at")
	private String updatedAt;

	public OneOffChargeResponse() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceInstanceId() {
		return serviceInstanceId;
	}

	public void setServiceInstanceId(String serviceInstanceId) {
		this.serviceInstanceId = serviceInstanceId;
	}

	public String getServiceInstanceType() {
		return serviceInstanceType;
	}

	public void setServiceInstanceType(String serviceInstanceType) {
		this.serviceInstanceType = serviceInstanceType;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public OneOffChargeResponse(Integer id, Integer requestId, String serviceId, String serviceInstanceId,
			String serviceInstanceType, String bcn, String chargeType, String description, String currency,
			Float amount, String createdAt, String status, String batchId, String updatedAt) {
		super();
		this.id = id;
		this.requestId = requestId;
		this.serviceId = serviceId;
		this.serviceInstanceId = serviceInstanceId;
		this.serviceInstanceType = serviceInstanceType;
		this.bcn = bcn;
		this.chargeType = chargeType;
		this.description = description;
		this.currency = currency;
		this.amount = amount;
		this.createdAt = createdAt;
		this.status = status;
		this.batchId = batchId;
		this.updatedAt = updatedAt;
	}

	@Override
	public String toString() {
		return "OneOffChargeResponse [id=" + id + ", requestId=" + requestId + ", serviceId=" + serviceId
				+ ", serviceInstanceId=" + serviceInstanceId + ", serviceInstanceType=" + serviceInstanceType + ", bcn="
				+ bcn + ", chargeType=" + chargeType + ", description=" + description + ", currency=" + currency
				+ ", amount=" + amount + ", createdAt=" + createdAt + ", status=" + status + ", batchId=" + batchId
				+ ", updatedAt=" + updatedAt + "]";
	}

}
