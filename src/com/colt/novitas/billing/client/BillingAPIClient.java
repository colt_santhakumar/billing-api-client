package com.colt.novitas.billing.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.json.JSONObject;

import com.colt.novitas.billing.client.request.BillBatchRequest;
import com.colt.novitas.billing.client.request.BilledChargeRequest;
import com.colt.novitas.billing.client.request.ModifyRecurringChargeRequest;
import com.colt.novitas.billing.client.request.OneOffChargeRequest;
import com.colt.novitas.billing.client.request.RecurringChargeRequest;
import com.colt.novitas.billing.client.response.ChargeResponse;
import com.colt.novitas.billing.client.response.OneOffChargeResponse;
import com.colt.novitas.billing.client.response.RecurringChargeResponse;
import com.colt.novitas.client.util.Callback;
import com.colt.novitas.client.util.HttpUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

public class BillingAPIClient {

	private String CREATE_ONE_OFF_CHARGES_URL = "http://amsnov02:8080/billing-api/api/one_off_charges";
	private String CREATE_RECURRING_CHARGES_URL = "http://amsnov02:8080/billing-api/api/recurring_charges";
	private String CREATE_BILL_BATCH_URL = "http://localhost:8080/billing-api/api/billing/batch";
	private String CREATE_BILLED_CHARGE_URL = "http://localhost:8080/billing-api/api/billing";

	private String REQUEST_API_USERNAME = "NovRequestUser59";
	private String REQUEST_API_PASSWORD = "NdxUCCd2Ff32jkve";
	
	private static final Gson GSON = new GsonBuilder().registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
		@Override
		public JsonElement serialize(Date src, Type srcType, JsonSerializationContext context) {

			DateFormat iso8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			iso8601Format.setTimeZone(TimeZone.getTimeZone("UTC"));

			return new JsonPrimitive(iso8601Format.format(src));
		}

	}).create();

	public BillingAPIClient() {
		super();
	}

	/**
	 * CREATE_ONE_OFF_CHARGES_URL = "http://localhost:8080/billing-api/api/one_off_charges";
	 * CREATE_RECURRING_CHARGES_URL = "http://localhost:8080/billing-api/api/recurring_charges";
	 * REQUEST_API_USERNAME = "NovRequestUser59";
	 * REQUEST_API_PASSWORD = "NdxUCCd2Ff32jkve";
	 * 
	 * @param cREATE_ONE_OFF_CHARGES_URL
	 * @param cREATE_RECURRING_CHARGES_URL
	 * @param rEQUEST_API_USERNAME
	 * @param rEQUEST_API_PASSWORD
	 */
	public BillingAPIClient(String cREATE_ONE_OFF_CHARGES_URL, String cREATE_RECURRING_CHARGES_URL,
			String rEQUEST_API_USERNAME, String rEQUEST_API_PASSWORD) {
		super();
		CREATE_ONE_OFF_CHARGES_URL = cREATE_ONE_OFF_CHARGES_URL;
		CREATE_RECURRING_CHARGES_URL = cREATE_RECURRING_CHARGES_URL;
		REQUEST_API_USERNAME = rEQUEST_API_USERNAME;
		REQUEST_API_PASSWORD = rEQUEST_API_PASSWORD;
	}
	
	
	/**
	 * CREATE_ONE_OFF_CHARGES_URL = "http://localhost:8080/billing-api/api/one_off_charges";
	 * CREATE_RECURRING_CHARGES_URL = "http://localhost:8080/billing-api/api/recurring_charges";
	 * CREATE_BILL_BATCH_URL = "http://localhost:8080/billing-api/api/billing/batch";
	 * CREATE_BILLED_CHARGE_URL = "http://localhost:8080/billing-api/api/billing";
	 * REQUEST_API_USERNAME = "NovRequestUser59";
	 * REQUEST_API_PASSWORD = "NdxUCCd2Ff32jkve";
	 * 
	 * @param cREATE_ONE_OFF_CHARGES_URL
	 * @param cREATE_RECURRING_CHARGES_URL
	 * @param cREATE_BILL_BATCH_URL
	 * @param cREATE_BILLED_CHARGE_URL
	 * @param rEQUEST_API_USERNAME
	 * @param rEQUEST_API_PASSWORD
	 */
	public BillingAPIClient(String cREATE_ONE_OFF_CHARGES_URL, String cREATE_RECURRING_CHARGES_URL,
			String cREATE_BILL_BATCH_URL, String cREATE_BILLED_CHARGE_URL, String rEQUEST_API_USERNAME,
			String rEQUEST_API_PASSWORD) {
		super();
		CREATE_ONE_OFF_CHARGES_URL = cREATE_ONE_OFF_CHARGES_URL;
		CREATE_RECURRING_CHARGES_URL = cREATE_RECURRING_CHARGES_URL;
		CREATE_BILL_BATCH_URL = cREATE_BILL_BATCH_URL;
		CREATE_BILLED_CHARGE_URL = cREATE_BILLED_CHARGE_URL;
		REQUEST_API_USERNAME = rEQUEST_API_USERNAME;
		REQUEST_API_PASSWORD = rEQUEST_API_PASSWORD;
	}

	
	public OneOffChargeResponse[] getOneOffChargesByServicesAndDate(String serviceInstanceId, String startDate,
			String endDate) {

		String url = CREATE_ONE_OFF_CHARGES_URL + "?service_instance_id=" + serviceInstanceId + "&start_date="
				+ startDate + "&end_date=" + endDate;

		final List<OneOffChargeResponse> finalOneOffChargeResponseList = new ArrayList<OneOffChargeResponse>();

		HttpUtils.connect(url, REQUEST_API_USERNAME, REQUEST_API_PASSWORD, new Callback() {

			@Override
			public void success(String response, int code) {

				List<OneOffChargeResponse> oneOffChargeResponse = GSON.fromJson(response,
						new TypeToken<List<OneOffChargeResponse>>() {
						}.getType());
				finalOneOffChargeResponseList.addAll(oneOffChargeResponse);
			}

			@Override
			public void failure(String response, int code) {
			}

			@Override
			public void failure(Exception e) {
			}

		});

		return finalOneOffChargeResponseList.toArray(new OneOffChargeResponse[finalOneOffChargeResponseList.size()]);
	}
	
	public OneOffChargeResponse[] getOneOffChargesByServiceIdAndDate(String serviceId, String startDate,
			String endDate) {

		String url = CREATE_ONE_OFF_CHARGES_URL + "?service_id=" + serviceId + "&start_date=" + startDate + "&end_date="
				+ endDate;

		final List<OneOffChargeResponse> finalOneOffChargeResponseList = new ArrayList<OneOffChargeResponse>();

		HttpUtils.connect(url, REQUEST_API_USERNAME, REQUEST_API_PASSWORD, new Callback() {

			@Override
			public void success(String response, int code) {

				List<OneOffChargeResponse> oneOffChargeResponse = GSON.fromJson(response,
						new TypeToken<List<OneOffChargeResponse>>() {
						}.getType());
				finalOneOffChargeResponseList.addAll(oneOffChargeResponse);
			}

			@Override
			public void failure(String response, int code) {
			}

			@Override
			public void failure(Exception e) {
			}

		});

		return finalOneOffChargeResponseList.toArray(new OneOffChargeResponse[finalOneOffChargeResponseList.size()]);
	}

	public Object createOneOffCharges(OneOffChargeRequest requestObject) {

		String url = CREATE_ONE_OFF_CHARGES_URL;

		HttpClient client = null;
		PostMethod postMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {
			client = new HttpClient();

			String authString = REQUEST_API_USERNAME + ":" + REQUEST_API_PASSWORD;
			byte[] authEncBytes = Base64.encodeBase64((byte[])authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			JSONObject jsonParam = new JSONObject();
			jsonParam.put("request_id", requestObject.getRequestId());
			jsonParam.put("service_id", requestObject.getServiceId());
			jsonParam.put("service_instance_id", requestObject.getServiceInstanceId());
			jsonParam.put("service_instance_type", requestObject.getServiceInstanceType());
			jsonParam.put("bcn", requestObject.getBcn());
			jsonParam.put("charge_type", requestObject.getChargeType());
			jsonParam.put("description", requestObject.getDescription());
			jsonParam.put("currency", requestObject.getCurrency());
			jsonParam.put("amount", requestObject.getAmount());
            jsonParam.put("service_instance_name", requestObject.getServiceInstanceName());
			// Send post request
			StringRequestEntity requestEntity = new StringRequestEntity(jsonParam.toString(), "application/json",
					"UTF-8");

			postMethod = new PostMethod(url);
			postMethod.setRequestEntity(requestEntity);

			// Add Headers
			postMethod.addRequestHeader("Content-Type", "application/json");
			postMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);

			int responseCode = client.executeMethod(postMethod);

			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("POST parameters : " + jsonParam);
			System.out.println("Response Code : " + responseCode);

			if (responseCode == 201) {
				correctResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				correctResponseReader.close();
				System.out.println(response.toString());
				return response.toString();

			} else if (responseCode == 204) {
				System.out.println("Successfully added OneOffCharges");
				return "SUCCESS";
			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				System.out.println(response.toString());

				ChargeResponse chargeResponse = new ChargeResponse();
				JSONObject jsonObj = new JSONObject(response.toString());
				if (!jsonObj.get("status_id").toString().equals("null")) {
					chargeResponse.setStatusId(jsonObj.getInt("status_id"));
				}
				if (!jsonObj.get("description").toString().equals("null")) {
					chargeResponse.setDescription(jsonObj.getString("description"));
				}

				return chargeResponse;
			}

		} catch (Exception e) {

		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return new Object();
	}
	
	public RecurringChargeResponse[] getRecurringChargesByServicesAndDate(String serviceInstanceId, String startDate,
			String endDate) {

		String url = CREATE_RECURRING_CHARGES_URL + "?service_instance_id=" + serviceInstanceId + "&start_date="
				+ startDate + "&end_date=" + endDate;

		final List<RecurringChargeResponse> finalRecurringChargeResponseList = new ArrayList<RecurringChargeResponse>();

		HttpUtils.connect(url, REQUEST_API_USERNAME, REQUEST_API_PASSWORD, new Callback() {

			@Override
			public void success(String response, int code) {

				List<RecurringChargeResponse> recurringChargeResponse = GSON.fromJson(response,
						new TypeToken<List<RecurringChargeResponse>>() {
						}.getType());
				finalRecurringChargeResponseList.addAll(recurringChargeResponse);
			}

			@Override
			public void failure(String response, int code) {
			}

			@Override
			public void failure(Exception e) {
			}

		});

		return finalRecurringChargeResponseList
				.toArray(new RecurringChargeResponse[finalRecurringChargeResponseList.size()]);
	}

	public Object createRecurringCharges(RecurringChargeRequest requestObject) {

		String url = CREATE_RECURRING_CHARGES_URL;

		HttpClient client = null;
		PostMethod postMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {

			client = new HttpClient();

			String authString = REQUEST_API_USERNAME + ":" + REQUEST_API_PASSWORD;
			byte[] authEncBytes = Base64.encodeBase64((byte[])authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			JSONObject jsonParam = new JSONObject();
			jsonParam.put("request_id", requestObject.getRequestId());
			jsonParam.put("service_id", requestObject.getServiceId());
			jsonParam.put("service_instance_id", requestObject.getServiceInstanceId());
			jsonParam.put("service_instance_type", requestObject.getServiceInstanceType());
			jsonParam.put("bcn", requestObject.getBcn());
			jsonParam.put("charge_type", requestObject.getChargeType());
			jsonParam.put("description", requestObject.getDescription());
			jsonParam.put("currency", requestObject.getCurrency());
			jsonParam.put("amount", requestObject.getAmount());
			jsonParam.put("frequency", requestObject.getFrequency());
			jsonParam.put("commitment_type", requestObject.getCommitmentType());
			jsonParam.put("commitment_expiry_date", requestObject.getCommitmentExpiryDate());
			jsonParam.put("service_instance_name", requestObject.getServiceInstanceName());
			// Send post request
			StringRequestEntity requestEntity = new StringRequestEntity(jsonParam.toString(), "application/json",
					"UTF-8");

			postMethod = new PostMethod(url);
			postMethod.setRequestEntity(requestEntity);

			// Add Headers
			postMethod.addRequestHeader("Content-Type", "application/json");
			postMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);

			int responseCode = client.executeMethod(postMethod);

			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("POST parameters : " + jsonParam);
			System.out.println("Response Code : " + responseCode);

			if (responseCode == 201) {
				correctResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				correctResponseReader.close();
				System.out.println(response.toString());
				return response.toString();
			} else if (responseCode == 204) {
				System.out.println("Successfully added RecurringCharges");
				return "SUCCESS";
			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				System.out.println(response.toString());

				ChargeResponse chargeResponse = new ChargeResponse();
				JSONObject jsonObj = new JSONObject(response.toString());
				if (!jsonObj.get("status_id").toString().equals("null")) {
					chargeResponse.setStatusId(jsonObj.getInt("status_id"));
				}
				if (!jsonObj.get("description").toString().equals("null")) {
					chargeResponse.setDescription(jsonObj.getString("description"));
				}

				return chargeResponse;
			}

		} catch (Exception e) {

		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();

			}
		}
		return new Object();
	}

	public Object endRecurringCharges(String serviceInstanceType, String serviceInstanceId) {

		String url = CREATE_RECURRING_CHARGES_URL + "?type=" + serviceInstanceType + "&id=" + serviceInstanceId;

		HttpClient client = null;
		DeleteMethod deleteMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {

			client = new HttpClient();

			String authString = REQUEST_API_USERNAME + ":" + REQUEST_API_PASSWORD;
			byte[] authEncBytes = Base64.encodeBase64((byte[])authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			// Send GET request
			deleteMethod = new DeleteMethod(url);

			// Add Headers
			deleteMethod.addRequestHeader("Content-Type", "application/json");
			deleteMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);

			int responseCode = client.executeMethod(deleteMethod);
			System.out.println("\nSending 'DELETE' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			if (responseCode == 201) {
				correctResponseReader = new BufferedReader(
						new InputStreamReader(deleteMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				correctResponseReader.close();

				System.out.println(response.toString());
				return response.toString();
			} else if (responseCode == 204) {
				System.out.println("Successfully ended the recurring charges");
				return "SUCCESS";
			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(deleteMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				System.out.println(response.toString());

				ChargeResponse chargeResponse = new ChargeResponse();
				JSONObject jsonObj = new JSONObject(response.toString());
				if (!jsonObj.get("status_id").toString().equals("null")) {
					chargeResponse.setStatusId(jsonObj.getInt("status_id"));
				}
				if (!jsonObj.get("description").toString().equals("null")) {
					chargeResponse.setDescription(jsonObj.getString("description"));
				}
				return chargeResponse;
			}

		} catch (Exception e) {

		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();

			}
		}
		return new Object();
	}

	public Object modifyRecurringCharges(String serviceInstanceType, String serviceInstanceId,
			ModifyRecurringChargeRequest requestObject) {

		String url = CREATE_RECURRING_CHARGES_URL + "?type=" + serviceInstanceType + "&id=" + serviceInstanceId;

		HttpClient client = null;
		PutMethod putMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {

			client = new HttpClient();

			String authString = REQUEST_API_USERNAME + ":" + REQUEST_API_PASSWORD;
			byte[] authEncBytes = Base64.encodeBase64((byte[])authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			JSONObject jsonParam = new JSONObject();
			jsonParam.put("request_id", requestObject.getRequestId());
			jsonParam.put("currency", requestObject.getCurrency());
			jsonParam.put("amount", requestObject.getAmount());
			jsonParam.put("frequency", requestObject.getFrequency());
			jsonParam.put("commitment_type", requestObject.getCommitmentType());
			jsonParam.put("commitment_expiry_date", requestObject.getCommitmentExpiryDate());
			
			// Send post request
			StringRequestEntity requestEntity = new StringRequestEntity(jsonParam.toString(), "application/json",
					"UTF-8");

			putMethod = new PutMethod(url);
			putMethod.setRequestEntity(requestEntity);

			// Add Headers
			putMethod.addRequestHeader("Content-Type", "application/json");
			putMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);

			int responseCode = client.executeMethod(putMethod);
			System.out.println("\nSending 'PUT' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			if (responseCode == 200) {
				correctResponseReader = new BufferedReader(new InputStreamReader(putMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				correctResponseReader.close();

				System.out.println(response.toString());
			} else if (responseCode == 204) {
				System.out.println("Successfully modified the recurring charges");
				return "SUCCESS";
			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(putMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				System.out.println(response.toString());

				ChargeResponse chargeResponse = new ChargeResponse();
				JSONObject jsonObj = new JSONObject(response.toString());
				if (!jsonObj.get("status_id").toString().equals("null")) {
					chargeResponse.setStatusId(jsonObj.getInt("status_id"));
				}
				if (!jsonObj.get("description").toString().equals("null")) {
					chargeResponse.setDescription(jsonObj.getString("description"));
				}
				return chargeResponse;
			}

		} catch (Exception e) {

		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();

			}
		}
		return new Object();
	}
	
	public Integer createBillBatch(BillBatchRequest billBatchRequest) {

		String url = CREATE_BILL_BATCH_URL;

		HttpClient client = null;
		PostMethod postMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {
			client = new HttpClient();

			String authString = REQUEST_API_USERNAME + ":" + REQUEST_API_PASSWORD;
			byte[] authEncBytes = Base64.encodeBase64((byte[])authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			JSONObject jsonParam = new JSONObject();
			jsonParam.put("bill_start_date", billBatchRequest.getBillStartDate());
			jsonParam.put("bill_end_date", billBatchRequest.getBillEndDate());
			jsonParam.put("file_name", billBatchRequest.getFileName());

			// Send post request
			StringRequestEntity requestEntity = new StringRequestEntity(jsonParam.toString(), "application/json",
					"UTF-8");

			postMethod = new PostMethod(url);
			postMethod.setRequestEntity(requestEntity);

			// Add Headers
			postMethod.addRequestHeader("Content-Type", "application/json");
			postMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);

			int responseCode = client.executeMethod(postMethod);

			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("POST parameters : " + jsonParam);
			System.out.println("Response Code : " + responseCode);

			if (responseCode == 200 || responseCode == 201) {
				correctResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				correctResponseReader.close();
				System.out.println(response.toString());
				
				System.out.println("Successfully added BillBatchRequest");
				return Integer.parseInt(response.toString());

			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				System.out.println(response.toString());
			}

		} catch (Exception e) {

		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();

			}
		}
		return -1;
	}
	
	public Integer createBilledCharge(BilledChargeRequest billedChargeRequest) {

		String url = CREATE_BILLED_CHARGE_URL;

		HttpClient client = null;
		PostMethod postMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {
			client = new HttpClient();

			String authString = REQUEST_API_USERNAME + ":" + REQUEST_API_PASSWORD;
			byte[] authEncBytes = Base64.encodeBase64((byte[])authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			JSONObject jsonParam = new JSONObject();
			jsonParam.put("batch_id", billedChargeRequest.getBatchId());
			jsonParam.put("bcn", billedChargeRequest.getBcn());
			jsonParam.put("country_code", billedChargeRequest.getCountryCode());
			jsonParam.put("novitas_service_id", billedChargeRequest.getNovitasServiceId());
			jsonParam.put("service_instance_id", billedChargeRequest.getServiceInstanceId());
			jsonParam.put("name", billedChargeRequest.getName());
			jsonParam.put("charge_type", billedChargeRequest.getChargeType());
			jsonParam.put("attribute", billedChargeRequest.getAttribute());
			jsonParam.put("port_capacity", billedChargeRequest.getPortCapacity());
			jsonParam.put("a_end_address", billedChargeRequest.getaEndAddress());
			jsonParam.put("b_end_address", billedChargeRequest.getbEndAddress());
			jsonParam.put("connection_bandwidth", billedChargeRequest.getConnectionBandwidth());
			jsonParam.put("amount", billedChargeRequest.getAmount());
			jsonParam.put("currency", billedChargeRequest.getCurrency());
			jsonParam.put("billing_start_date", billedChargeRequest.getBillingStartDate());
			jsonParam.put("billing_end_date", billedChargeRequest.getBillingEndDate());
			jsonParam.put("sub_charge_type", billedChargeRequest.getSubChargeType());
			jsonParam.put("frequency", billedChargeRequest.getFrequency());
			jsonParam.put("commitment_type", billedChargeRequest.getCommitmentType());
			jsonParam.put("total_amount", billedChargeRequest.getTotalAmount());
			jsonParam.put("csp_name", billedChargeRequest.getCspName());
			jsonParam.put("b_end_address_secondary", billedChargeRequest.getbEndAddressSecondary());
			
			// Send post request
			StringRequestEntity requestEntity = new StringRequestEntity(jsonParam.toString(), "application/json",
					"UTF-8");

			postMethod = new PostMethod(url);
			postMethod.setRequestEntity(requestEntity);

			// Add Headers
			postMethod.addRequestHeader("Content-Type", "application/json");
			postMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);

			int responseCode = client.executeMethod(postMethod);

			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("POST parameters : " + jsonParam);
			System.out.println("Response Code : " + responseCode);

			if (responseCode == 200 || responseCode == 201) {
				correctResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				correctResponseReader.close();
				System.out.println(response.toString());
				
				System.out.println("Successfully added BilledChargeRequest");
				return Integer.parseInt(response.toString());

			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				System.out.println(response.toString());
			}

		} catch (Exception e) {

		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();

			}
		}
		return -1;
	}
	
	public String undoOneOffCharges(Integer chargeId) {

		String url = CREATE_ONE_OFF_CHARGES_URL+"/"+chargeId;

		HttpClient client = null;
		DeleteMethod deleteMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {

			client = new HttpClient();

			String authString = REQUEST_API_USERNAME + ":" + REQUEST_API_PASSWORD;
			byte[] authEncBytes = Base64.encodeBase64((byte[])authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			// Send GET request
			deleteMethod = new DeleteMethod(url);

			// Add Headers
			deleteMethod.addRequestHeader("Content-Type", "application/json");
			deleteMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);

			int responseCode = client.executeMethod(deleteMethod);
			System.out.println("\nSending 'DELETE' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			if (responseCode == 201) {
				correctResponseReader = new BufferedReader(
						new InputStreamReader(deleteMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				correctResponseReader.close();

				System.out.println(response.toString());
				return response.toString();
			} else if (responseCode == 204) {
				System.out.println("Successfully roll backed oneoff charges");
				return "SUCCESS";
			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(deleteMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				System.out.println(response.toString());
				
				return null;
			}

		} catch (Exception e) {
			return null;
		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public String undoRecurringCharges(Integer chargeeId) {

		String url = CREATE_RECURRING_CHARGES_URL + "/"+chargeeId+"/undo";

		HttpClient client = null;
		PostMethod postMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {
			client = new HttpClient();

			String authString = REQUEST_API_USERNAME + ":" + REQUEST_API_PASSWORD;
			byte[] authEncBytes = Base64.encodeBase64((byte[])authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			// Send Post request
			postMethod = new PostMethod(url);

			// Add Headers
			postMethod.addRequestHeader("Content-Type", "application/json");
			postMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);

			int responseCode = client.executeMethod(postMethod);
			System.out.println("\nSending 'DELETE' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			if (responseCode == 201) {
				correctResponseReader = new BufferedReader(
						new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				correctResponseReader.close();

				System.out.println(response.toString());
				return response.toString();
			} else if (responseCode == 204) {
				System.out.println("Successfully ended the recurring charges");
				return "SUCCESS";
			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				System.out.println(response.toString());
				
				return null;
			}

		} catch (Exception e) {
			return null;
		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
