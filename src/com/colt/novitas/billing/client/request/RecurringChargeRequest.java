package com.colt.novitas.billing.client.request;

import java.io.Serializable;
import java.util.Date;

public class RecurringChargeRequest implements Serializable {

	private static final long serialVersionUID = 1727361245169158516L;

	private Integer requestId;
	private String serviceId;
	private String serviceInstanceId;
	private String serviceInstanceType;
	private String bcn;
	private String chargeType;
	private String description;
	private String currency;
	private Float amount;
	private String frequency;
	private String commitmentType;
	private String commitmentExpiryDate;
    private String serviceInstanceName;
    
	public RecurringChargeRequest() {
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceInstanceId() {
		return serviceInstanceId;
	}

	public void setServiceInstanceId(String serviceInstanceId) {
		this.serviceInstanceId = serviceInstanceId;
	}

	public String getServiceInstanceType() {
		return serviceInstanceType;
	}

	public void setServiceInstanceType(String serviceInstanceType) {
		this.serviceInstanceType = serviceInstanceType;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	/**
	 * @return the commitmentType
	 */
	public String getCommitmentType() {
		return commitmentType;
	}

	/**
	 * @param commitmentType the commitmentType to set
	 */
	public void setCommitmentType(String commitmentType) {
		this.commitmentType = commitmentType;
	}

	/**
	 * @return the commitmentExpiryDate
	 */
	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	/**
	 * @param commitmentExpiryDate the commitmentExpiryDate to set
	 */
	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}

	/**
	 * @return the serviceInstanceName
	 */
	public String getServiceInstanceName() {
		return serviceInstanceName;
	}

	/**
	 * @param serviceInstanceName the serviceInstanceName to set
	 */
	public void setServiceInstanceName(String serviceInstanceName) {
		this.serviceInstanceName = serviceInstanceName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RecurringChargeRequest [requestId=" + requestId
				+ ", serviceId=" + serviceId + ", serviceInstanceId="
				+ serviceInstanceId + ", serviceInstanceType="
				+ serviceInstanceType + ", bcn=" + bcn + ", chargeType="
				+ chargeType + ", description=" + description + ", currency="
				+ currency + ", amount=" + amount + ", frequency=" + frequency
				+ ", commitmentType=" + commitmentType
				+ ", commitmentExpiryDate=" + commitmentExpiryDate
				+ ", serviceInstanceName=" + serviceInstanceName + "]";
	}

	
	

}
