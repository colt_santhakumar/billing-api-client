package com.colt.novitas.billing.client.request;

import java.io.Serializable;

public class BillBatchRequest implements Serializable {

	private static final long serialVersionUID = -779707974852016701L;

	private String billStartDate;
	private String billEndDate;
	private String fileName;

	public BillBatchRequest() {
	}

	public String getBillStartDate() {
		return billStartDate;
	}

	public void setBillStartDate(String billStartDate) {
		this.billStartDate = billStartDate;
	}

	public String getBillEndDate() {
		return billEndDate;
	}

	public void setBillEndDate(String billEndDate) {
		this.billEndDate = billEndDate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String toString() {
		return "BillBatchRequest [billStartDate=" + billStartDate + ", billEndDate=" + billEndDate + ", fileName="
				+ fileName + "]";
	}

}