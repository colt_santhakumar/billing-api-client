package com.colt.novitas.billing.client.request;

import java.io.Serializable;
import java.util.Date;

public class ModifyRecurringChargeRequest implements Serializable {

	private static final long serialVersionUID = -3273366372238113684L;

	private Integer requestId;
	private String currency;
	private Float amount;
	private String frequency;
	private String commitmentType;
	private String commitmentExpiryDate;

	public ModifyRecurringChargeRequest() {
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	/**
	 * @return the commitmentType
	 */
	public String getCommitmentType() {
		return commitmentType;
	}

	/**
	 * @param commitmentType the commitmentType to set
	 */
	public void setCommitmentType(String commitmentType) {
		this.commitmentType = commitmentType;
	}

	/**
	 * @return the commitmentExpiryDate
	 */
	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	/**
	 * @param commitmentExpiryDate the commitmentExpiryDate to set
	 */
	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ModifyRecurringChargeRequest [requestId=" + requestId
				+ ", currency=" + currency + ", amount=" + amount
				+ ", frequency=" + frequency + ", commitmentType="
				+ commitmentType + ", commitmentExpiryDate="
				+ commitmentExpiryDate + "]";
	}

	

}
