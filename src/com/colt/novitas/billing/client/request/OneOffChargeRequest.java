package com.colt.novitas.billing.client.request;

import java.io.Serializable;

public class OneOffChargeRequest implements Serializable {

	private static final long serialVersionUID = 3322549067817050287L;

	private Integer requestId;
	private String serviceId;
	private String serviceInstanceId;
	private String serviceInstanceType;
	private String bcn;
	private String chargeType;
	private String description;
	private String currency;
	private Float amount;
	private String serviceInstanceName;

	public OneOffChargeRequest() {
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceInstanceId() {
		return serviceInstanceId;
	}

	public void setServiceInstanceId(String serviceInstanceId) {
		this.serviceInstanceId = serviceInstanceId;
	}

	public String getServiceInstanceType() {
		return serviceInstanceType;
	}

	public void setServiceInstanceType(String serviceInstanceType) {
		this.serviceInstanceType = serviceInstanceType;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}
    
	
	/**
	 * @return the serviceInstanceName
	 */
	public String getServiceInstanceName() {
		return serviceInstanceName;
	}

	/**
	 * @param serviceInstanceName the serviceInstanceName to set
	 */
	public void setServiceInstanceName(String serviceInstanceName) {
		this.serviceInstanceName = serviceInstanceName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OneOffChargeRequest [requestId=" + requestId + ", serviceId="
				+ serviceId + ", serviceInstanceId=" + serviceInstanceId
				+ ", serviceInstanceType=" + serviceInstanceType + ", bcn="
				+ bcn + ", chargeType=" + chargeType + ", description="
				+ description + ", currency=" + currency + ", amount=" + amount
				+ ", serviceInstanceName=" + serviceInstanceName + "]";
	}

	

}
