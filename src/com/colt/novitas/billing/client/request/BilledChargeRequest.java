package com.colt.novitas.billing.client.request;

import java.io.Serializable;

public class BilledChargeRequest implements Serializable {

	private static final long serialVersionUID = -1552528406770337062L;

	private Integer batchId;
	private String bcn;
	private String countryCode;
	private String novitasServiceId;
	private String serviceInstanceId;
	private String name;
	private String chargeType;
	private String attribute;
	private String portCapacity;
	private String aEndAddress;
	private String bEndAddress;
	private String connectionBandwidth;
	private Float amount;
	private String currency;
	private String billingStartDate;
	private String billingEndDate;
	private String commitmentType;
	private String subChargeType;
	private String frequency;
	private Float totalAmount;
	private String cspName;
    private String bEndAddressSecondary;

	public BilledChargeRequest() {
	}

	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getNovitasServiceId() {
		return novitasServiceId;
	}

	public void setNovitasServiceId(String novitasServiceId) {
		this.novitasServiceId = novitasServiceId;
	}

	public String getServiceInstanceId() {
		return serviceInstanceId;
	}

	public void setServiceInstanceId(String serviceInstanceId) {
		this.serviceInstanceId = serviceInstanceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getPortCapacity() {
		return portCapacity;
	}

	public void setPortCapacity(String portCapacity) {
		this.portCapacity = portCapacity;
	}

	public String getaEndAddress() {
		return aEndAddress;
	}

	public void setaEndAddress(String aEndAddress) {
		this.aEndAddress = aEndAddress;
	}

	public String getbEndAddress() {
		return bEndAddress;
	}

	public void setbEndAddress(String bEndAddress) {
		this.bEndAddress = bEndAddress;
	}

	public String getConnectionBandwidth() {
		return connectionBandwidth;
	}

	public void setConnectionBandwidth(String connectionBandwidth) {
		this.connectionBandwidth = connectionBandwidth;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getBillingStartDate() {
		return billingStartDate;
	}

	public void setBillingStartDate(String billingStartDate) {
		this.billingStartDate = billingStartDate;
	}

	public String getBillingEndDate() {
		return billingEndDate;
	}

	public void setBillingEndDate(String billingEndDate) {
		this.billingEndDate = billingEndDate;
	}

	public String getCommitmentType() {
		return commitmentType;
	}

	public void setCommitmentType(String commitmentType) {
		this.commitmentType = commitmentType;
	}

	public String getSubChargeType() {
		return subChargeType;
	}

	public void setSubChargeType(String subChargeType) {
		this.subChargeType = subChargeType;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public Float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getCspName() {
		return cspName;
	}

	public void setCspName(String cspName) {
		this.cspName = cspName;
	}

	public String getbEndAddressSecondary() {
		return bEndAddressSecondary;
	}

	public void setbEndAddressSecondary(String bEndAddressSecondary) {
		this.bEndAddressSecondary = bEndAddressSecondary;
	}

	@Override
	public String toString() {
		return "BilledChargeRequest [batchId=" + batchId + ", bcn=" + bcn
				+ ", countryCode=" + countryCode + ", novitasServiceId="
				+ novitasServiceId + ", serviceInstanceId=" + serviceInstanceId
				+ ", name=" + name + ", chargeType=" + chargeType
				+ ", attribute=" + attribute + ", portCapacity=" + portCapacity
				+ ", aEndAddress=" + aEndAddress + ", bEndAddress="
				+ bEndAddress + ", connectionBandwidth=" + connectionBandwidth
				+ ", amount=" + amount + ", currency=" + currency
				+ ", billingStartDate=" + billingStartDate
				+ ", billingEndDate=" + billingEndDate + ", commitmentType="
				+ commitmentType + ", subChargeType=" + subChargeType
				+ ", frequency=" + frequency + ", totalAmount=" + totalAmount
				+ ", cspName=" + cspName + ", bEndAddressSecondary="
				+ bEndAddressSecondary + "]";
	}

	

}
